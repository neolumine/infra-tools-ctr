# base image last checked for updates as of: 2021-12-11
FROM ubuntu:focal-20211006
LABEL org.opencontainers.image.authors="neolumine@gmail.com"

# (bump the datestamp on the following line to force a cache invalidation on the apt lists)
# apt lists updated as of: 2021-12-11T15:33Z

RUN apt-get update

## install prerequisites and helpful builtin packages
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y git wget unzip

## install tools

# tool versions last updated 2021-12-02 to 2021-12-11

ARG KUBECTL_VERSION=1.22.4
ARG HELM_VERSION=3.7.1
ARG MINIO_CLIENT_VERSION=2021-12-10T00-14-28Z
ARG ARGO_CLIENT_VERSION=3.2.4
ARG FLUX_CLIENT_VERSION=0.24.0
ARG GOVC_VERSION=0.27.2
ARG TERRAFORM_VERSION=1.1.0

# ansible
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y ansible

# kubectl
RUN cd /tmp && \
    wget -O kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    install -v kubectl /usr/local/bin && \
    rm kubectl

# helm
RUN cd /tmp && \
    wget -O helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    tar xf helm.tar.gz && \
    install -v linux-amd64/helm /usr/local/bin && \
    rm helm.tar.gz

# minio client
RUN cd /tmp && \
    wget -O mc https://dl.min.io/client/mc/release/linux-amd64/archive/mc.RELEASE.${MINIO_CLIENT_VERSION} && \
    install -v mc /usr/local/bin && \
    rm mc

# argo client
RUN cd /tmp && \
    wget -O argo-linux-amd64.gz https://github.com/argoproj/argo-workflows/releases/download/v${ARGO_CLIENT_VERSION}/argo-linux-amd64.gz && \
    gunzip argo-linux-amd64.gz && \
    install -v argo-linux-amd64 /usr/local/bin/argo && \
    rm argo-linux-amd64

# flux client
RUN cd /tmp && \
    wget -O flux.tar.gz https://github.com/fluxcd/flux2/releases/download/v0.24.0/flux_${FLUX_CLIENT_VERSION}_linux_amd64.tar.gz && \
    tar xf flux.tar.gz && \
    install -v flux /usr/local/bin/flux && \
    rm flux

# govc
RUN cd /tmp && \
    wget -O govc.tar.gz https://github.com/vmware/govmomi/releases/download/v${GOVC_VERSION}/govc_Linux_x86_64.tar.gz && \
    tar xf govc.tar.gz && \
    install -v govc /usr/local/bin && \
    rm govc.tar.gz

# terraform
RUN cd /tmp && \
    wget -O terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform.zip && \
    install -v terraform /usr/local/bin && \
    rm terraform terraform.zip

## non-root user setup

RUN chmod g=u /etc/passwd
COPY docker/entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
USER 10240
WORKDIR /work
CMD ["/bin/bash"]
