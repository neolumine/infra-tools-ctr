_my_dir=$(cd $(dirname ${BASH_SOURCE[0]})/ && pwd)

alias build-infra-tools-ctr="${_my_dir}/scripts/build"
alias infra-tools-ctr-run="${_my_dir}/scripts/run"