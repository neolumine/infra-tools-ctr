#!/bin/bash

# https://docs.openshift.com/container-platform/3.3/creating_images/guidelines.html
# "Support Arbitrary User IDs"
if ! whoami &> /dev/null; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:${HOME}:/bin/bash" >> /etc/passwd
  else
    echo "entrypoint.sh: warning: /etc/passwd not writable"
  fi
fi

exec "$@"