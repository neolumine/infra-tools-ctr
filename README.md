# infra-tools-ctr

Container image with useful infrastructure tools

## Features

This image is based on Ubuntu 20.04.

### Run script

The `run` script has the following features:

* Run inside the container as a normal (non-root) user with the same UID as the user on the host
    * Create an entry in `/etc/passwd` in the container for a user with matching name and UID as on the host
    * Mount a `tmpfs` on the user home directory in the container so that writing to this directory is possible (e.g. for tools that create cache files in the home directory)
* Bind mount common subdirectories of `$HOME` into the container for using SSH, `kubectl`, and `helm`
* Pass through common environment variables for general shell use and for `kubectl`
* Forward a range of TCP ports (9000-9999) to the host to use with tools such as `kubectl port-forward`

## Contents

#### Prerequisites and standard packages

* `git`
* `wget`
* `unzip`

#### Tools

* `ansible` (installed from standard repositories)
* `kubectl`
* `helm`
* `mc` (Minio client)
* `argo` (Argo Workflows client)
* `flux` (FluxCD v2 client)
* `govc` (Go vCenter CLI)
* `terraform`

## Usage

Build by running `docker build` (or `podman build` or other tool as desired) or by using the convenience script:

```
scripts/build
```

This script names the resulting image `infra-tools`.

The `build` script may be run from any directory and will have the same behaviour.

Use the convenience script to open a shell in a temporary container created from the `infra-tools` image:

```
scripts/run
```

Or, use it to run a single command in a temporary container created from the above image:

```
scripts/run <command>
```

#### Shell Aliases

Load the following shell aliases by running `source aliases.sh`:

* `build-infra-tools-ctr`: corresponding to `scripts/build`
* `infra-tools-ctr-run`: corresponding to `scripts/run`

## Maintenance Status

This repository is made public for informational and demonstration purposes only and may change in any way or disappear at any time. It should be considered **unmaintained**.

## License

TODO